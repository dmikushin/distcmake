# distcmake: an evolution of distcc

This is an agent program for abstract [Distributed CMake](https://gitlab.kitware.com/dmikushin/cmake), which maps compilation and linking jobs executed on the main machine onto the network machines farm.

## Prerequisites

The main machine environment must define the following variables:

* `CMAKE_DIST_SERVER_URL` - a distributed CMake server, usually running on the host machine, and accessible from within the main docker container
* `CMAKE_DIST_USERNAME` - a username, on behalf of which the worker machines could be reached by SSH

The main machine should provide configuration file `CMAKE_SOURCE_DIR/cmakedist.json` or `CMAKE_BINARY_DIR/cmakedist.json` with the following content:

```json
{
	"docker_image" : "ubuntu:latest",
	"machines" : [ "machine1", "machine2", "machine3" ]
}
```

The machine names must refer to the names defined in `.ssh/config` fo the corresponing user and be accessible without an interactive password.

## Further changes needed in Distributed CMake

1. First two launcher arguments should be the `CMAKE_SOURCE_DIR` and `CMAKE_BINARY_DIR`
2. Add CMake target that always runs first as `distcmake --start CMAKE_SOURCE_DIR CMAKE_BINARY_DIR`
3. Add CMake cache variable which counts all objects for compilation

## Further standalone testing needed

1. In order to avoid dealing with filesystem drivers, create a preloaded library with LIBC open/stat/close implementation that shall act as a virtual filesystem.
2. Ensure preloaded library sucessfully hooks all missing files on the remote filesystem, and returns modified files back to the main node.

