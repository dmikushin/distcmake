#ifndef DIST_CMAKE_H
#define DIST_CMAKE_H

#include <string>
#include <vector>

enum ServerCommand
{
	ServerCommandStartSession,
	ServerCommandSubmitJob
};

struct ServerStatus
{
	int errCode;
	std::string stdErr;
	std::string stdOut;
};

struct SessionDescriptor
{
	std::string username;
	std::string cmakeSourceDir;
	std::string cmakeBinaryDir;
	std::string dockerImage;
	std::vector<std::string> machines; 
};

// TODO Singleton.
class Server
{
	Server();

public :

        Server(const Server&) = delete;

        Server& operator=(const Server&) = delete;

        static Server& get();

	static const int Port = 4422;

	std::string url;

	ServerStatus start(const SessionDescriptor& desc);
	
	ServerStatus submit(const std::string& cmakeSourceDir, const std::string& cmakeBinaryDir,
		const std::vector<std::string>& args);
};

#endif // DIST_CMAKE_H

