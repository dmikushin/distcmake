#ifndef SSH_CONNECTION_H
#define SSH_CONNECTION_H

#include <string>
#include <vector>

class Subprocess;

class SSHConnection
{	
	bool connected = false;

	Subprocess* subprocess = nullptr;

public :

	bool isConnected() const;

	SSHConnection(const char* server);
	
	~SSHConnection();

	int sync(std::vector<char>& result);
	
	// Issue the specified command to the SSH session (newline is added),
	// and return the resulting pair of status code and console output.
	int run(std::string command, std::pair<int, std::string>& result);
};

#endif // SSH_CONNECTION_H

