#ifndef SUBPROCESS_H
#define SUBPROCESS_H

#include <sys/types.h>

class Subprocess
{
	pid_t childPid;
	int fromChild, toChild;

	bool connected = false;
	
public :

	bool isConnected() const;

	Subprocess(const char* command);
	
	~Subprocess();
	
	int read(char* buffer, size_t szbuffer);
	
	int write(const char* buffer, size_t szbuffer);
	
	bool isAlive();
};

#endif // SUBPROCESS_H

