#include "distcmake.h"

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <filesystem>
#include <string>

namespace fs = std::filesystem;

Server::Server()
{
	url = "localhost";
	const char* cmake_dist_server_url = getenv("CMAKE_DIST_SERVER_URL");
	if (cmake_dist_server_url) url = cmake_dist_server_url;
}

Server& Server::get()
{
	static Server server;
	return server;
}

ServerStatus Server::start(const SessionDescriptor& desc)
{
}

ServerStatus Server::submit(const std::string& cmakeSourceDir, const std::string& cmakeBinaryDir,
	const std::vector<std::string>& args)
{
}

// distcmake --start CMAKE_SOURCE_DIR CMAKE_BINARY_DIR
int start(int argc, char* argv[])
{
	if (argc < 4)
	{
		fprintf(stderr, "distcmake --start must be followed by 2 arguments: CMAKE_SOURCE_DIR and CMAKE_BINARY_DIR\n");
		return -1;
	}
	
	// Collect CMake source + binary folders from the arguments.
	std::string cmakeSourceDir = argv[2];
	std::string cmakeBinaryDir = argv[3]; 

	// TODO Locate distcmake.json in CMAKE_SOURCE_DIR or CMAKE_BINARY_DIR.
	for (auto dir : { cmakeSourceDir, cmakeBinaryDir })
	{
		if (!fs::exists(fs::path(dir) / "distcmake.json"))
			continue;
		
		break;
	}
	
	// TODO Reads docker image name from the file, as well as the machines names
	std::string dockerImage;
	std::vector<std::string> machines;

	// Get the user name from environment variable CMAKE_DIST_USERNAME or USER.
	std::string username;
	{
		const char* cmake_dist_username = getenv("CMAKE_DIST_USERNAME");
		if (cmake_dist_username) username = cmake_dist_username;
		else
		{
			const char* user = getenv("USER");
			if (user) username = user;
		}
	}

	// Send the new session descriptor to the server (must respond with 200).
	SessionDescriptor desc
	{
		username,
		cmakeSourceDir,
		cmakeBinaryDir,
		dockerImage,
		machines
	};
	
	auto status = Server::get().start(desc);
	fprintf(stdout, "%s", status.stdOut.c_str());
	fprintf(stderr, "%s", status.stdErr.c_str());
	if (status.errCode != 200)
		return -1;

	return 0;
}

// distcmake g++ source.cpp -c -o object.o
int submit(int argc, char* argv[])
{
	std::string cmakeSourceDir = argv[1];
	std::string cmakeBinaryDir = argv[2];
	std::vector<std::string> args;
	for (int i = 3; i < argc; i++)
		args.push_back(argv[i]);

	// Pass command-line to the server and busy-wait for response.
	auto status = Server::get().submit(cmakeSourceDir, cmakeBinaryDir, args);
	fprintf(stdout, "%s", status.stdOut.c_str());
	fprintf(stderr, "%s", status.stdErr.c_str());

	return status.errCode;
}

int main(int argc, char* argv[])
{
	if (argc == 1)
	{
		printf("Usage: distcmake [--start] ARGS ...\n");
		return 1;
	}
	
	if (!strcmp(argv[1], "--start"))
		return start(argc, argv);

	return submit(argc, argv);
}

