#include "distcmake.h"

#include <cstdlib>
#include <map>

struct SessionConnection
{
	// TODO
};

// The index of opened connections.
static std::map<std::pair<std::string, std::string>, SessionConnection> connections;

static ServerStatus start(const SessionDescriptor& desc)
{
	ServerStatus status;

	for (auto& machine : desc.machines)
	{
		// TODO Check remote SSH connection to the machine
	
		// TODO Start docker container with the specified image, providing SSHD as an entry point
		// (We *require* the Docker image to come with an SSH server).
		
		// TODO Record the hash of the container managed by us.
	}
		
	// TODO Export CMAKE_SOURCE_DIR to NFS.
	
	// TODO Export CMAKE_BINARY_DIR to NFS as well, if it is not contained in CMAKE_SOURCE_DIR
	
	for (auto& machine : desc.machines)
	{
		// TODO Connect the docker container by SSH with NFS PortForwarding
		// and keep the connection open
		
		// TODO Create CMAKE_SOURCE_DIR and mount NFS via SSH to it,
		// as shown here: https://gist.github.com/proudlygeek/5721498
	}
	
	// TODO Attach the opened SSH connections information to the session descriptor. 

	return status;
}

static ServerStatus submit(const std::string& cmakeSourceDir, const std::string& cmakeBinaryDir,
	const std::vector<std::string>& args)
{
	ServerStatus status;
	
	auto it = connections.find(std::make_pair(cmakeSourceDir, cmakeBinaryDir));
	if (it == connections.end())
	{
		status.stdErr = "Job belongs to an unknown session, cannot submit, aborting";
		status.errCode = -1;
		return status;
	}
	
	// TODO Choose a worker and send the command to it.
	auto& connection = it->second;
	
	return status;
}

int main(int argc, char* argv[])
{
	while (1)
	{
		// TODO Starts in the background and listens to the port 4422

		// TODO handle requests.
		ServerCommand command;
		ServerStatus status;
		switch (command)
		{
		case ServerCommandStartSession :
			{
				// TODO
				SessionDescriptor desc;
				status = start(desc);
			}
			break;
		case ServerCommandSubmitJob :
			{
				// TODO
				std::string cmakeSourceDir, cmakeBinaryDir;
				std::vector<std::string> args;
				status = submit(cmakeSourceDir, cmakeBinaryDir, args);
			}
			break;
		}
		
		// TODO Return status receipt to the user.
	}

	return 0;
}

