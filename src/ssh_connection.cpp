#include "ssh_connection.h"
#include "subprocess.h"

#include <chrono>
#include <cstdio>
#include <cstring>
#include <sstream>
#include <thread>

class Terminator
{
	std::string expression;
	std::string check;

public :

	Terminator()
	{
		std::stringstream ss1;
		ss1 << "echo $((";
		int64_t a = rand();
		ss1 << a;
		ss1 << " + ";
		int64_t b = rand();
		ss1 << b;
		ss1 << "))\n";
		expression = ss1.str();
		
		std::stringstream ss2;
		ss2 << (a + b);
		check = ss2.str();
	}
	
	const char* data()
	{
		return expression.c_str();
	}
	
	size_t size()
	{
		return expression.size();
	}
	
	const char* result()
	{
		return check.c_str();
	}
};

bool SSHConnection::isConnected() const { return connected; }

SSHConnection::SSHConnection(const char* server)
{
	const char* sshCmdTemplate = "ssh"
		" -o StrictHostKeyChecking=no"
		" -o ServerAliveInterval=10"
		" -o ServerAliveCountMax=3"
		" -o ExitOnForwardFailure=yes"
		" -o StreamLocalBindUnlink=yes"
		" -o PasswordAuthentication=no ";
	
	// Synthesize SSH command.
	std::stringstream ss;
	ss << sshCmdTemplate;
	ss << server;
	
	// Run SSH client with stdin/stdout redirection.
	const std::string sshCmd = ss.str();
	subprocess = new Subprocess(sshCmd.c_str());
	if (!subprocess->isConnected()) return;

	// Ensure SSH connection has been properly established.		
	std::vector<char> output;
	if (sync(output) != 0)
	{
		fprintf(stderr, "%s", output.data());
		return;
	}
	
	connected = true;
}
	
SSHConnection::~SSHConnection()
{
	delete subprocess;
}

int SSHConnection::sync(std::vector<char>& result)
{
	std::vector<char> output;
	output.resize(1);
	output[0] = '\0';
	Terminator terminator;
	subprocess->write(terminator.data(), terminator.size());
	while (1)
	{
		char buf[128];
		int size = subprocess->read(buf, sizeof(buf));
		if (size > 0)
		{
			output.resize(output.size() + size);
			output[output.size() - 1] = '\0';
			memcpy(output.data() + output.size() - size - 1, buf, size);
		}
		
		char* finished = strstr(output.data(), terminator.result());
		if (finished)
		{
			*finished = '\0';
			result.assign(output.data(), finished + 1); 
			break;
		}
		
		// If process returns an error, exit with that failure.
		if (!subprocess->isAlive()) return -1;
		
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
	}

	return 0;
}
	
int SSHConnection::run(std::string command, std::pair<int, std::string>& result)
{
	if (!connected) return -1;
	
	const std::string commandWithTerminator = command + "; echo \"Result is $?\"\n";
	subprocess->write(commandWithTerminator.data(), commandWithTerminator.size());
	
	std::vector<char> output;
	if (sync(output) != 0)
	{
		fprintf(stderr, "%s", output.data());
		return -1;
	}
	
	char* found = strstr(output.data(), "Result is ");
	if (!found) return -1;
	int retCode = atoi(found + strlen("Result is "));
	result = std::make_pair(retCode, std::string(output.data(), found));
	
	return 0;
}

