#include "subprocess.h"

// Based on http://media.unpythonic.net/emergent-files/01108826729/popen2.c

#include <cstdio>
#include <cstdlib>
#include <sys/wait.h>
#include <unistd.h>

bool Subprocess::isConnected() const { return connected; }

Subprocess::Subprocess(const char* command)
{
	pid_t p;
	int pipeStdin[2], pipeStdout[2];

	if (pipe(pipeStdin)) return;
	if (pipe(pipeStdout)) return;

	p = fork();
	
	if (p < 0) return; // Fork failed
	
	// If is a child
	if (p == 0)
	{
		close(pipeStdin[1]);
		dup2(pipeStdin[0], 0);
		close(pipeStdout[0]);
		dup2(pipeStdout[1], 1);
		execl("/bin/sh", "sh", "-c", command, NULL);
		perror("execl"); exit(99);
	}

	childPid = p;
	toChild = pipeStdin[1];
	fromChild = pipeStdout[0];
	
	close(pipeStdin[0]);
	close(pipeStdout[1]);
	
	connected = true;
}

Subprocess::~Subprocess()
{
	close(toChild);
	kill(childPid, 0); 
}

int Subprocess::read(char* buffer, size_t szbuffer)
{
	if (!connected) return -1;
	
	return ::read(fromChild, buffer, szbuffer);
}

int Subprocess::write(const char* buffer, size_t szbuffer)
{
	if (!connected) return -1;
	
	return ::write(toChild, buffer, szbuffer);
}

bool Subprocess::isAlive()
{
	if (!connected) return -1;

	if (waitpid(childPid, NULL, WNOHANG) == -1)
		return false;
	
	return true;
}

