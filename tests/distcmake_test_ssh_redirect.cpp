#include "ssh_connection.h"

int main(int argc, char* argv[])
{
	SSHConnection connection("vinet");
	std::pair<int, std::string> result;
	if (connection.run("uname -a", result) == 0)
	{
		printf("%s", result.second.c_str());
		printf("%d\n", result.first);		
	}
	if (connection.run("docker --version", result) == 0)
	{
		printf("%s", result.second.c_str());
		printf("%d\n", result.first);		
	}
	if (connection.run("somethingnotexisting", result) == 0)
	{
		printf("%s", result.second.c_str());
		printf("%d\n", result.first);		
	}
	return 0;
}

